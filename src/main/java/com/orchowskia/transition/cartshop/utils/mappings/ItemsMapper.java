package com.orchowskia.transition.cartshop.utils.mappings;

import com.orchowskia.transition.cartshop.api.views.item.ItemView;
import com.orchowskia.transition.cartshop.domain.objects.Item;

import java.util.List;
import java.util.stream.Collectors;

public class ItemsMapper {
    public static List<ItemView> toItemViews(List<Item> items) {
        return items.stream().map(ItemsMapper::toItemView).collect(Collectors.toList());
    }

    public static ItemView toItemView(Item item) {
        var view = new ItemView();
        view.setPublicId(item.getPublicId());
        view.setDescription(item.getDescription());
        view.setName(item.getName());
        view.setPriceUsd(item.getPriceUsd());
        return view;
    }
}
