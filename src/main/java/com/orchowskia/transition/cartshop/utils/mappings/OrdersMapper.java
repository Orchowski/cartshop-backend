package com.orchowskia.transition.cartshop.utils.mappings;

import com.orchowskia.transition.cartshop.api.views.item.OrderSummaryResponse;
import com.orchowskia.transition.cartshop.api.views.item.OrderView;
import com.orchowskia.transition.cartshop.domain.objects.Order;
import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.orchowskia.transition.cartshop.utils.mappings.ItemsMapper.toItemView;

public class OrdersMapper {
    public static OrderSummaryResponse toOrderSummaryResponse(OrderSummary orderSummary) {
        var response = new OrderSummaryResponse();
        response.setActive(orderSummary.isActive());
        response.setOrderOwnerId(orderSummary.getOrderOwnerId());
        response.setOrderViews(toOrderViews(orderSummary.getOrders()));
        response.setTotalPrice(orderSummary.getTotalPrice());
        response.setPublicId(orderSummary.getPublicId());
        return response;
    }

    public static List<OrderView> toOrderViews(List<Order> orders) {
        return orders == null ? new ArrayList<>() :
                orders.stream().map(OrdersMapper::toOrderView).collect(Collectors.toList());
    }


    public static OrderView toOrderView(Order order) {
        var view = new OrderView();
        view.setItem(toItemView(order.getItem()));
        view.setPublicId(order.getPublicId());
        view.setQuantity(order.getQuantity());
        return view;
    }
}
