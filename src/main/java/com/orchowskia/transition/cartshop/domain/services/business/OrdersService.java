package com.orchowskia.transition.cartshop.domain.services.business;

import com.orchowskia.transition.cartshop.domain.objects.Item;
import com.orchowskia.transition.cartshop.domain.objects.Order;
import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;
import com.orchowskia.transition.cartshop.domain.objects.SimplyOrderSummary;
import com.orchowskia.transition.cartshop.domain.services.ItemStorage;
import com.orchowskia.transition.cartshop.domain.services.OrderStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {
    private final OrderStorage orderStorage;
    private final ItemStorage itemStorage;

    @Autowired
    public OrdersService(OrderStorage orderStorage, ItemStorage itemStorage) {
        this.orderStorage = orderStorage;
        this.itemStorage = itemStorage;
    }

    public OrderSummary getActiveOrder(String userId) {
        return orderStorage.getActiveOrder(userId).orElseGet(() -> orderStorage.createNewOrderFor(userId));
    }

    public Optional<OrderSummary> updateOrderByItem(String userId, String itemId, int amount) {
        var currentOrder = getActiveOrder(userId);
        var itemToAdd = itemStorage.getById(itemId);
        if (!itemToAdd.isPresent()) {
            return Optional.empty();
        }
        var item = itemToAdd.get();
        BigDecimal newTotalPrice = calculatePrice(item, amount, currentOrder);
        return Optional.of(orderStorage.addItem(currentOrder.getPublicId(), currentOrder.getPublicId(), item.getPublicId(), amount, newTotalPrice));
    }

    public void placeOrder(String userId) {
        var currentOrder = getActiveOrder(userId);
        orderStorage.placeOrder(currentOrder.getPublicId());
    }

    private BigDecimal calculatePrice(Item item, int amount, OrderSummary currentOrder) {
        var last = currentOrder.getOrders().stream().filter(order -> order.getItem().getPublicId().equals(item.getPublicId())).findFirst();
        var partialPrice = last.map(order -> new BigDecimal(amount - order.getQuantity()))
                .map(quantity -> quantity.multiply(item.getPriceUsd()))
                .orElse(item.getPriceUsd().multiply(new BigDecimal(amount)));
        return currentOrder.getTotalPrice().add(partialPrice);
    }

    public List<SimplyOrderSummary> getAll(String userId, boolean admin) {
        return admin ? orderStorage.getAllOrdersList() :
                orderStorage.getOrdersList(userId);
    }
}
