package com.orchowskia.transition.cartshop.domain.services.business;

import com.orchowskia.transition.cartshop.domain.objects.Items;
import com.orchowskia.transition.cartshop.domain.services.ItemStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ItemsService {

    private final ItemStorage itemStorage;

    @Autowired
    public ItemsService(ItemStorage itemStorage){
        this.itemStorage = itemStorage;
    }

    public Items getAll(){
        return itemStorage.getAll(Optional.empty());
    }
}
