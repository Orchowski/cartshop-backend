package com.orchowskia.transition.cartshop.domain.services;

import com.orchowskia.transition.cartshop.domain.objects.Item;
import com.orchowskia.transition.cartshop.domain.objects.Items;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ItemStorage {
    Items getAll(Optional<Pageable> page);
    Optional<Item> getById(String id);
}
