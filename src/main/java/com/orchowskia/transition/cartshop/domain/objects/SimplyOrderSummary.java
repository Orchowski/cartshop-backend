package com.orchowskia.transition.cartshop.domain.objects;

import java.math.BigDecimal;

public interface SimplyOrderSummary {
    String getPublicId();
    BigDecimal getTotalPrice();
    String getOwnerEmail();
}
