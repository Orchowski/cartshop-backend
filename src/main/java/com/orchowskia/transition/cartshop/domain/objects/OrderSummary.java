package com.orchowskia.transition.cartshop.domain.objects;

import java.math.BigDecimal;
import java.util.List;

public interface OrderSummary {
    String getOrderOwnerId();

    String getPublicId();

    BigDecimal getTotalPrice();

    boolean isActive();

    List<Order> getOrders();
}
