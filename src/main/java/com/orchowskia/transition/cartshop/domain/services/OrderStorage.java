package com.orchowskia.transition.cartshop.domain.services;

import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;
import com.orchowskia.transition.cartshop.domain.objects.SimplyOrderSummary;
import org.hibernate.loader.plan.build.internal.returns.ScalarReturnImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface OrderStorage {
    Optional<OrderSummary> getActiveOrder(String userId);

    OrderSummary createNewOrderFor(String userId);

    OrderSummary addItem(String userId, String orderId, String itemPublicId, int amount, BigDecimal newTotalPrice);

    void placeOrder(String orderId);

    List<SimplyOrderSummary> getOrdersList(String userId);
    List<SimplyOrderSummary> getAllOrdersList();
}
