package com.orchowskia.transition.cartshop.domain.objects;

public interface Order {
    String getPublicId();
    Item getItem();
    long getQuantity();
}
