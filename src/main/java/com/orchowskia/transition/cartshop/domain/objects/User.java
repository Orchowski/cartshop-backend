package com.orchowskia.transition.cartshop.domain.objects;

public interface User {
    String getPublicId();
    String getEmail();
    String getPassword();
    String getRole();
}
