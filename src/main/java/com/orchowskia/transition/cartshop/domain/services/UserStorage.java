package com.orchowskia.transition.cartshop.domain.services;

import com.orchowskia.transition.cartshop.domain.objects.User;

import java.util.Optional;

public interface UserStorage {
    Optional<User> getUser(String email);
}
