package com.orchowskia.transition.cartshop.domain.objects;

import java.util.List;

public interface Items {
    List<Item> getContent();
}
