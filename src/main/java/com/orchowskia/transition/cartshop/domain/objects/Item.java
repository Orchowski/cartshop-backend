package com.orchowskia.transition.cartshop.domain.objects;

import java.math.BigDecimal;

public interface Item {
    String getName();
    BigDecimal getPriceUsd();
    String getDescription();
    String getPublicId();
}
