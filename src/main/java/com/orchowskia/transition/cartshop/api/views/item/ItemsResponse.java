package com.orchowskia.transition.cartshop.api.views.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orchowskia.transition.cartshop.domain.objects.Item;
import com.orchowskia.transition.cartshop.domain.objects.Items;

import java.util.List;
import java.util.stream.Collectors;

import static com.orchowskia.transition.cartshop.utils.mappings.ItemsMapper.toItemViews;

public class ItemsResponse {
    private final List<ItemView> data;

    public ItemsResponse(Items items) {
        this(items.getContent());
    }

    public ItemsResponse(List<Item> items) {
        this.data = toItemViews(items);
    }

    public List<ItemView> getData() {
        return data;
    }
}
