package com.orchowskia.transition.cartshop.api.endpoints;

import com.orchowskia.transition.cartshop.api.views.item.ItemView;
import com.orchowskia.transition.cartshop.api.views.item.ItemsResponse;
import com.orchowskia.transition.cartshop.domain.services.business.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/items")
public class ItemsController {
    private final ItemsService itemsService;

    @Autowired
    public ItemsController(ItemsService itemsService) {
        this.itemsService = itemsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ItemsResponse> getAll() {
        var items = itemsService.getAll();
        var response = new ItemsResponse(items);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
