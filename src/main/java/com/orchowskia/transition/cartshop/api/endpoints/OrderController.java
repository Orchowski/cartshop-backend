package com.orchowskia.transition.cartshop.api.endpoints;

import com.orchowskia.transition.cartshop.api.requests.AddItemRequest;
import com.orchowskia.transition.cartshop.api.security.AuthRequired;
import com.orchowskia.transition.cartshop.api.security.Context;
import com.orchowskia.transition.cartshop.api.views.item.OrderSummaryResponse;
import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;
import com.orchowskia.transition.cartshop.domain.objects.SimplyOrderSummary;
import com.orchowskia.transition.cartshop.domain.services.business.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.List;

import static com.orchowskia.transition.cartshop.utils.mappings.OrdersMapper.toOrderSummaryResponse;

@RestController
//@Secured({"ROLE_USER"})
@RequestMapping("/orders")
public class OrderController {
    private final Context context;
    private final OrdersService ordersService;

    //    private final OrderServo
    @Autowired
    public OrderController(Context context, OrdersService ordersService) {
        this.context = context;
        this.ordersService = ordersService;
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public ResponseEntity<OrderSummaryResponse> getCurrentOrder() {
        var order = ordersService.getActiveOrder(context.getId());
        return new ResponseEntity<>(toOrderSummaryResponse(order), HttpStatus.OK);
    }

    @AuthRequired
    @RequestMapping(value = "/current", method = RequestMethod.PATCH)
    public ResponseEntity<OrderSummaryResponse> addItem(@Valid @RequestBody AddItemRequest addItemRequest) {
        var order = ordersService.updateOrderByItem(context.getId(), addItemRequest.getItemId(), addItemRequest.getAmount());
        if (order.isPresent()) {
            return new ResponseEntity<>(toOrderSummaryResponse(order.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @AuthRequired
    @RequestMapping(value = "/current/place", method = RequestMethod.PATCH)
    public ResponseEntity placeOrder() {
        ordersService.placeOrder(context.getId());

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @AuthRequired
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SimplyOrderSummary>> getAllOrders() {
        var result = ordersService.getAll(context.getId(),context.isAdmin());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
