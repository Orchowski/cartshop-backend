package com.orchowskia.transition.cartshop.api.security;

import com.orchowskia.transition.cartshop.storage.repositories.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class AuthInterceptor implements HandlerInterceptor {
    @Autowired
    private Context context;
    @Autowired
    private UsersRepository repository;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        String authToken = request.getHeader("Authorization");

        if (handler instanceof HandlerMethod) {
            if (!verifyAuthorization(authToken) && ((HandlerMethod) handler).hasMethodAnnotation(AuthRequired.class)) {
                response.sendError(401, "unauthorized operation");
            }
            if (!context.isAdmin()
                    && ((HandlerMethod) handler).hasMethodAnnotation(AdminRequired.class)) {
                response.sendError(403, "forbidden operation");
            }
        }
        return true;
    }

    private boolean verifyAuthorization(String authToken) {

        try {
            if (authToken != null && authToken.toLowerCase().startsWith("basic")) {
                String base64Credentials = authToken.substring("Basic".length()).trim();
                byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
                String credentials = new String(credDecoded, StandardCharsets.UTF_8);
                final String[] values = credentials.split(":", 2);
                var username = values[0];
                var password = values[1];
                var nullableUser = repository.findByEmail(username);
                if (nullableUser.isPresent()) {
                    var user = nullableUser.get();
                    if (user.getPassword().equals(password)) {
                        context.setId(user.getPublicId());
                        context.setAdmin(user.getRole().equals("ADMIN"));
                        context.setEmail(user.getEmail());
                        return true;
                    }
                }
            }

        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
