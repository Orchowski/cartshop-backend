package com.orchowskia.transition.cartshop.api.views.item;

import java.math.BigDecimal;
import java.util.List;

public class OrderSummaryResponse {
    private String publicId;
    private boolean active;
    private String orderOwnerId;
    private List<OrderView> orderViews;
    private BigDecimal totalPrice;

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getOrderOwnerId() {
        return orderOwnerId;
    }

    public void setOrderOwnerId(String orderOwnerId) {
        this.orderOwnerId = orderOwnerId;
    }

    public List<OrderView> getOrderViews() {
        return orderViews;
    }

    public void setOrderViews(List<OrderView> orderViews) {
        this.orderViews = orderViews;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
