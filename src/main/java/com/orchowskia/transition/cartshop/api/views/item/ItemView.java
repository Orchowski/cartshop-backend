package com.orchowskia.transition.cartshop.api.views.item;

import com.orchowskia.transition.cartshop.domain.objects.Item;

import java.math.BigDecimal;

public class ItemView implements Item {
    private String name;
    private BigDecimal priceUsd;
    private String description;
    private String publicId;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPriceUsd() {
        return priceUsd;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getPublicId() {
        return publicId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPriceUsd(BigDecimal priceUsd) {
        this.priceUsd = priceUsd;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }
}
