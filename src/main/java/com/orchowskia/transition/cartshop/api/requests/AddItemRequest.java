package com.orchowskia.transition.cartshop.api.requests;

import javax.validation.constraints.Min;

public class AddItemRequest {
    private String itemId;
    @Min(0)
    private int amount;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
