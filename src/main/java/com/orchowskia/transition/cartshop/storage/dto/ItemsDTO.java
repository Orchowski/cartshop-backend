package com.orchowskia.transition.cartshop.storage.dto;

import com.orchowskia.transition.cartshop.domain.objects.Item;
import com.orchowskia.transition.cartshop.domain.objects.Items;

import java.util.List;

public class ItemsDTO implements Items {
    private final List<Item> items;

    public ItemsDTO(List<Item> items) {
        this.items = items;
    }

    @Override
    public List<Item> getContent() {
        return items;
    }
}
