package com.orchowskia.transition.cartshop.storage.entities;

import afu.org.checkerframework.checker.oigj.qual.O;
import com.orchowskia.transition.cartshop.domain.objects.Order;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "order")
public class OrderEntity implements Order {
    @Id
    @Column(name = "public_id", updatable = false, nullable = false)
    private String publicId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aggregate", nullable = false)
    private OrderSummaryEntity summary;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "item_id")
    private ItemEntity item;

    @Column(name = "item_quantity", nullable = false)
    private long quantity;

    @Override
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @Override
    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }

    public OrderSummaryEntity getSummary() {
        return summary;
    }

    public void setSummary(OrderSummaryEntity summary) {
        this.summary = summary;
    }

    @Override
    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
