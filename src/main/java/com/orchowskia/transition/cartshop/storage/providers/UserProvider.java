package com.orchowskia.transition.cartshop.storage.providers;

import com.orchowskia.transition.cartshop.domain.objects.User;
import com.orchowskia.transition.cartshop.domain.services.UserStorage;
import com.orchowskia.transition.cartshop.storage.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserProvider implements UserStorage {
    private UsersRepository usersRepository;

    @Autowired
    public UserProvider(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public Optional<User> getUser(String email) {
        return usersRepository.findByEmail(email).map(user -> (User) user);
    }
}
