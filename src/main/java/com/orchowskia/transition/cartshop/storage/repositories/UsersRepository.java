package com.orchowskia.transition.cartshop.storage.repositories;

import com.orchowskia.transition.cartshop.storage.entities.ItemEntity;
import com.orchowskia.transition.cartshop.storage.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<UserEntity, String>, JpaSpecificationExecutor<UserEntity> {
    Optional<UserEntity> findByEmail(String email);

}
