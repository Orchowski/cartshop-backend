package com.orchowskia.transition.cartshop.storage.repositories;

import com.orchowskia.transition.cartshop.domain.services.OrderStorage;
import com.orchowskia.transition.cartshop.storage.entities.ItemEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ItemsRepository extends CrudRepository<ItemEntity, String>, JpaSpecificationExecutor<ItemEntity> {

}
