package com.orchowskia.transition.cartshop.storage.entities;

import com.orchowskia.transition.cartshop.domain.objects.Order;
import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;
import org.apache.catalina.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "order_summary")
public class OrderSummaryEntity implements OrderSummary {
    @Id
    @Column(name = "public_id", updatable = false, nullable = false)
    private String publicId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity owner;

    @OneToMany(
            mappedBy = "summary",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private Set<OrderEntity> orders = new HashSet<>();

    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice;

    @Column(name = "active", nullable = false)
    private boolean isActive;

    @Override
    public String getOrderOwnerId() {
        return owner.getPublicId();
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public List<Order> getOrders() {
        return new ArrayList<>(orders);
    }

    public Set<OrderEntity> getOrderEntities() {
        return orders;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
