package com.orchowskia.transition.cartshop.storage.providers;

import com.google.common.collect.Lists;
import com.orchowskia.transition.cartshop.domain.objects.Item;
import com.orchowskia.transition.cartshop.domain.objects.Items;
import com.orchowskia.transition.cartshop.domain.services.ItemStorage;
import com.orchowskia.transition.cartshop.storage.dto.ItemsDTO;
import com.orchowskia.transition.cartshop.storage.repositories.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class ItemProvider implements ItemStorage {

    private final ItemsRepository itemsRepository;

    @Autowired
    public ItemProvider(ItemsRepository itemsRepository) {
        this.itemsRepository = itemsRepository;
    }

    @Override
    // TODO PUZZLE : Handle pagination
    public Items getAll(Optional<Pageable> page) {
        List<Item> items = Lists.newArrayList(page.map(p -> itemsRepository.findAll())
                .orElse(itemsRepository.findAll()));
        return new ItemsDTO(items);
    }

    @Override
    public Optional<Item> getById(String id) {
        return itemsRepository.findById(id).map(x -> x);
    }
}
