package com.orchowskia.transition.cartshop.storage.entities;

import com.orchowskia.transition.cartshop.domain.objects.User;
import com.orchowskia.transition.cartshop.domain.services.UserStorage;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class UserEntity implements User {
    @Id
    @Column(name = "public_id", updatable = false, nullable = false)
    private String publicId;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "role", nullable = false)
    private String role;


    @Column(name = "secret", nullable = false)
    private String secret;

    @OneToMany(
            mappedBy = "owner",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<OrderSummaryEntity> orders;

    @Override
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Set<OrderSummaryEntity> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderSummaryEntity> orders) {
        this.orders = orders;
    }

    @Override
    public String getPassword() {
        return secret;
    }

    @Override
    public String getRole() {
        return role;
    }
}
