package com.orchowskia.transition.cartshop.storage.repositories;

import com.orchowskia.transition.cartshop.storage.entities.OrderEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository  extends CrudRepository<OrderEntity, String> {
}
