package com.orchowskia.transition.cartshop.storage.repositories;

import com.orchowskia.transition.cartshop.storage.entities.ItemEntity;
import com.orchowskia.transition.cartshop.storage.entities.OrderSummaryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OrderSummaryRepository extends CrudRepository<OrderSummaryEntity, String> {
    Optional<OrderSummaryEntity> findByIsActiveTrueAndOwnerPublicId(String publicId);
    List<OrderSummaryEntity> findByIsActiveFalseAndOwnerPublicId(String publicId);
}
