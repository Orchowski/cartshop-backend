package com.orchowskia.transition.cartshop.storage.entities;

import com.orchowskia.transition.cartshop.domain.objects.Item;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "item")
public class ItemEntity implements Item {
    @Id
    @Column(name = "public_id", updatable = false, nullable = false)
    private String publicId;

    @OneToMany(mappedBy = "item")
    private Set<OrderEntity> orders;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price_usd", nullable = false)
    private BigDecimal priceUsd;

    @Column(name = "description")
    private String description;

    @Override
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BigDecimal getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(BigDecimal priceUsd) {
        this.priceUsd = priceUsd;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<OrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderEntity> orders) {
        this.orders = orders;
    }
}
