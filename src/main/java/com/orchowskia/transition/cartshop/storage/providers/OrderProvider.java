package com.orchowskia.transition.cartshop.storage.providers;

import com.google.common.collect.Lists;
import com.orchowskia.transition.cartshop.domain.objects.OrderSummary;
import com.orchowskia.transition.cartshop.domain.objects.SimplyOrderSummary;
import com.orchowskia.transition.cartshop.domain.services.OrderStorage;
import com.orchowskia.transition.cartshop.storage.entities.OrderEntity;
import com.orchowskia.transition.cartshop.storage.entities.OrderSummaryEntity;
import com.orchowskia.transition.cartshop.storage.repositories.ItemsRepository;
import com.orchowskia.transition.cartshop.storage.repositories.OrderRepository;
import com.orchowskia.transition.cartshop.storage.repositories.OrderSummaryRepository;
import com.orchowskia.transition.cartshop.storage.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class OrderProvider implements OrderStorage {
    private final OrderSummaryRepository orderSummaryRepository;
    private final UsersRepository usersRepository;
    private final ItemsRepository itemsRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public OrderProvider(OrderSummaryRepository orderSummaryRepository,
                         UsersRepository usersRepository,
                         ItemsRepository itemsRepository,
                         OrderRepository orderRepository) {
        this.orderSummaryRepository = orderSummaryRepository;
        this.usersRepository = usersRepository;
        this.itemsRepository = itemsRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public Optional<OrderSummary> getActiveOrder(String userId) {
        return orderSummaryRepository.findByIsActiveTrueAndOwnerPublicId(userId).map(x -> x);
    }

    @Override
    public OrderSummary createNewOrderFor(String userId) {
        var newOne = new OrderSummaryEntity();
        newOne.setActive(true);
        newOne.setOwner(usersRepository.findById(userId).orElseThrow());
        newOne.setPublicId(UUID.randomUUID().toString());
        newOne.setTotalPrice(BigDecimal.ZERO);
        orderSummaryRepository.save(newOne);
        return newOne;
    }

    @Transactional
    @Override
    public OrderSummary addItem(String userId, String orderId, String itemPublicId, int amount, BigDecimal newTotalPrice) {

        var item = itemsRepository.findById(itemPublicId).orElseThrow();
        var orderSummary = orderSummaryRepository.findById(orderId).orElseThrow();

        var orderItem = orderSummary.getOrderEntities().stream().filter(o -> o.getItem().getPublicId().equals(itemPublicId)).findFirst();
        if (orderItem.isPresent()) {
            var order = orderItem.get();
            order.setItem(item);
            order.setQuantity(amount);
            orderRepository.save(order);
        } else {
            var order = new OrderEntity();
            order.setItem(item);
            order.setQuantity(amount);
            order.setPublicId(UUID.randomUUID().toString());
            order.setSummary(orderSummary);
            orderRepository.save(order);
            orderSummary.getOrderEntities().add(order);
        }

        orderSummary.setTotalPrice(newTotalPrice);
        return orderSummaryRepository.save(orderSummary);

    }

    @Override
    public void placeOrder(String orderId) {
        orderSummaryRepository.findById(orderId)
                .ifPresent((summary) -> {
                    summary.setActive(false);
                    orderSummaryRepository.save(summary);
                });
    }

    @Override
    public List<SimplyOrderSummary> getOrdersList(String userId) {
        return orderSummaryRepository.findByIsActiveFalseAndOwnerPublicId(userId).stream()
                .map(this::toSimplySummary).collect(Collectors.toList());
    }

    @Override
    public List<SimplyOrderSummary> getAllOrdersList() {
        var orders = Lists.newArrayList(orderSummaryRepository.findAll());
        return orders.stream()
                .map(this::toSimplySummary).collect(Collectors.toList());
    }

    private SimplyOrderSummary toSimplySummary(OrderSummaryEntity summary) {
        return new SimplyOrderSummary() {
            @Override
            public String getPublicId() {
                return summary.getPublicId();
            }

            @Override
            public BigDecimal getTotalPrice() {
                return summary.getTotalPrice();
            }

            @Override
            public String getOwnerEmail() {
                return summary.getOwner().getEmail();
            }
        };
    }
}
