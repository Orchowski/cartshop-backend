package com.orchowskia.transition.cartshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(CartshopApplication.class, args);
    }
}
