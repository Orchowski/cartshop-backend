INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('ce9369ec-cdee-4686-a41f-f6e0a0b06278', 'Pipe extra large', 10.99, 'Awesome pipe, just for now - extra cheap!');

INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('a5479113-5290-45d5-8e9c-9faea5756361', 'Pipe short', 7.99, 'Awesome pipe, just for now - extra cheap!');

INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('4b3a5177-0696-49a7-8d00-1a436eeb2b34', '5x Screw 4,5mm', 2.99, 'Awesome screws, made of titan!');

INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('6d49aea7-d438-4e25-9355-f9be3f7df03d', '10x Screw 4,5mm', 4.99, 'Awesome screws, made of titan!');

INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('5b5e069f-639f-4c37-aaa8-5ca222cfde0f', '10x Screw 2,5mm', 2.99, 'Awesome screws, made of titan!');

INSERT INTO shop.item
(public_id, "name", price_usd, description)
VALUES('9931de1a-fdec-4edd-8308-b11f5bf08166', '5x Screw 2,5mm', 1.99, 'Awesome screws, made of titan!');

INSERT INTO shop."user"
(public_id, email, "role", secret)
VALUES('18fbd204-e6b5-490a-8179-204aff1fdb37', 'orchowskialeksander@gmail.com', 'ADMIN', 'qwerty');

INSERT INTO shop."user"
(public_id, email, "role", secret)
VALUES('45bdc2f4-dcce-41ac-8ea4-798490e2fd56', 'comodsuda@gmail.com', 'USER', 'qwerty');