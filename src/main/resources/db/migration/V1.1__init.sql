-- TODO: Everything should work on private ids
--USERS
create
  table
  "user"( public_id varchar(38) not null unique,
          email varchar(255) not null,
          "role" varchar(15) not null ,
          secret text not null,
          constraint PK_User primary key (public_id));
--ITEMS
create
  table
  "item"( public_id varchar(38) not null unique,
          "name" varchar(255) not null,
          price_usd decimal(12, 2) not null,
          description text,
          constraint PK_Item primary key (public_id));
--ORDER SUMMARIES
create
  table
  "order_summary"( public_id varchar(38) not null unique,
                   user_id varchar(38) not null,
                   total_price decimal(12, 2) not null,
                   active boolean not null,
                   constraint FK_Summary_User foreign key (user_id) references "user"(public_id),
                   constraint PK_OrderSummaries primary key (public_id));;
--ORDERS
create
  table
  "order"( public_id varchar(38) not null unique,
           "aggregate" varchar(38) not null,
           item_id varchar(38) not null,
           item_quantity int not null,
           constraint FK_Order_Item foreign key (item_id) references "item"(public_id),
           constraint FK_Order_Summary foreign key ("aggregate") references "order_summary"(public_id),
           constraint PK_Order primary key (public_id));